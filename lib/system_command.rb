# frozen_string_literal: true

require 'colorized_string'
require 'shellwords'

# Allows you to run external process with syntax a little bit more polished than
# the ruby built-ins.
#
# Turn an external process into a class:
#
#     git = SystemCommand.new('git')
#     git.run('status')
class SystemCommand
  Error = Class.new(StandardError)

  class << self
    def verbose?
      @verbose ||= false
    end

    attr_writer :verbose

    def run?(*, **)
      argv0, *argv = coerce_argv(*)

      puts_command argv0, argv
      system([argv0, argv0], *argv, **)
    end

    def run(*, **)
      argv0, *argv = coerce_argv(*)

      puts_command argv0, argv
      system([argv0, argv0], *argv, **)
      raise SystemCommand::Error, "#{argv0} failed (#{Process.last_status})" unless Process.last_status.success?

      nil
    end

    def gets(*, **)
      argv0, *argv = coerce_argv(*)

      puts_command argv0, argv
      result = IO.popen([argv0, *argv], **, &:read)
      raise SystemCommand::Error, "#{argv0} failed (#{Process.last_status})" unless Process.last_status.success?

      result
    end

    def lines(*, chomp: false, **) = gets(*, **).lines(chomp:)

    def each_line(*, chomp: false, **, &)
      argv0, *argv = coerce_argv(*)
      raise LocalJumpError, 'no block given' unless block_given?

      puts_command argv0, argv
      IO.popen([argv0, *argv], **) do |io|
        io.each_line(chomp:, &)
      end
      raise SystemCommand::Error, "#{argv0} failed (#{Process.last_status})" unless Process.last_status.success?

      nil
    end

    private

    def coerce_argv(*args)
      args = Array(args.first) if args.length == 1
      raise ArgumentError, 'Must specify one or more argument' if args.empty?

      args.map do |arg|
        arg = arg.to_path if arg.respond_to?(:to_path)
        arg
      end
    end

    def puts_command(argv0, argv)
      puts ColorizedString["$ #{[argv0, *argv].shelljoin}"].light_black if verbose?
    end
  end

  def initialize(path, *options)
    @path = path
    @options = options
  end

  def run?(...) = SystemCommand.run?(@path, *@options, ...)
  def run(...) = SystemCommand.run(@path, *@options, ...)
  def gets(...) = SystemCommand.gets(@path, *@options, ...)
  def lines(...) = SystemCommand.lines(@path, *@options, ...)
  def each_line(...) = SystemCommand.each_line(@path, *@options, ...)
end

require_relative 'system_command/version'
