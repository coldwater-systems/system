# frozen_string_literal: true

require 'pathname'
require 'system_command'
SystemCommand.verbose = true

RSpec.describe SystemCommand do
  let(:not_executable) { File.expand_path('not_executable', __dir__) }

  describe 'self.run?' do
    it 'runs commands, printing them to the console', :aggregate_failures do
      pipe_r, pipe_w = IO.pipe

      result = SystemCommand.run? 'echo', 'just  testing', out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to eq "just  testing\n"
      expect(result).to be true
    end

    it 'accepts an array of arguments', :aggregate_failures do
      pipe_r, pipe_w = IO.pipe

      result = SystemCommand.run? ['echo', 'just  testing'], out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to eq "just  testing\n"
      expect(result).to be true
    end

    it 'accepts Pathname values in arguments', :aggregate_failures do
      pipe_r, pipe_w = IO.pipe

      result = SystemCommand.run? 'cat', Pathname(not_executable), out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to match(/\A# This file/)
      expect(result).to be true
    end

    it 'returns false if the command exits unsuccessfully' do
      result = SystemCommand.run? 'false'

      expect(result).to be false
    end

    it "returns nil if the command doesn't exist" do
      result = SystemCommand.run? '__missing_command__'

      expect(result).to be nil
    end

    it 'returns nil if the command is not executable' do
      result = SystemCommand.run? not_executable

      expect(result).to be nil
    end

    it 'raises if no arguments are provided' do
      expect { SystemCommand.run? }.to raise_error(ArgumentError, 'Must specify one or more argument')
    end
  end

  describe 'self.run' do
    it 'runs commands, printing them to the console' do
      pipe_r, pipe_w = IO.pipe

      SystemCommand.run 'echo', 'just  testing', out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to eq "just  testing\n"
    end

    it 'accepts an array of arguments' do
      pipe_r, pipe_w = IO.pipe

      SystemCommand.run ['echo', 'just  testing'], out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to eq "just  testing\n"
    end

    it 'accepts Pathname values in arguments' do
      pipe_r, pipe_w = IO.pipe

      SystemCommand.run 'cat', Pathname(not_executable), out: pipe_w

      pipe_w.close
      expect(pipe_r.read).to match(/\A# This file/)
    end

    it 'raises if the command exits unsuccessfully' do
      expect { SystemCommand.run 'false', 'ignored_arg' }.to raise_error(
        SystemCommand::Error, /\Afalse failed \(pid \d+ exit 1\)\z/
      )
    end

    it "raises if the command doesn't exist" do
      expect { SystemCommand.run '__missing_command__' }.to raise_error(
        SystemCommand::Error, /\A__missing_command__ failed \(pid \d+ exit 127\)\z/
      )
    end

    it 'raises if the command is not executable' do
      expect { SystemCommand.run not_executable }.to raise_error(
        SystemCommand::Error, %r{.*/not_executable failed \(pid \d+ exit 127\)\z}
      )
    end

    it 'raises if no arguments are provided' do
      expect { SystemCommand.run }.to raise_error(ArgumentError, 'Must specify one or more argument')
    end
  end

  describe 'self.gets' do
    it 'runs commands, returning the output' do
      expect(SystemCommand.gets('echo', 'just  testing')).to eq "just  testing\n"
    end

    it 'accepts an array of arguments' do
      expect(SystemCommand.gets(['echo', 'just  testing'])).to eq "just  testing\n"
    end

    it 'accepts Pathname values in arguments' do
      expect(SystemCommand.gets('cat', Pathname(not_executable))).to match(/\A# This file/)
    end

    it 'raises if the command exits unsuccessfully' do
      expect { SystemCommand.gets 'false', 'ignored_arg' }.to raise_error(
        SystemCommand::Error, /\Afalse failed \(pid \d+ exit 1\)\z/
      )
    end

    it "raises if the command doesn't exist" do
      expect { SystemCommand.gets '__missing_command__' }.to raise_error(
        Errno::ENOENT, 'No such file or directory - __missing_command__'
      )
    end

    it 'raises if the command is not executable' do
      expect { SystemCommand.gets not_executable }.to raise_error(
        Errno::EACCES, %r{\APermission denied - .*/not_executable\z}
      )
    end

    it 'raises if no arguments are provided' do
      expect { SystemCommand.gets }.to raise_error(ArgumentError, 'Must specify one or more argument')
    end
  end

  describe 'self.each_line' do
    it 'runs commands, yielding each line of the output' do
      expect {|b| SystemCommand.each_line('echo', 'just  testing', &b) }.to yield_with_args("just  testing\n")
    end

    it 'accepts an array of arguments' do
      expect {|b| SystemCommand.each_line(['echo', 'just  testing'], &b) }.to yield_with_args("just  testing\n")
    end

    it 'accepts Pathname values in arguments' do
      expect {|b| SystemCommand.each_line 'cat', Pathname(not_executable), &b }.to yield_with_args(/\A# This file/)
    end

    it 'raises if the command exits unsuccessfully' do
      expect do
        SystemCommand.each_line('false', 'ignored_arg') { flunk 'unexpected yield' }
      end.to raise_error(SystemCommand::Error, /\Afalse failed \(pid \d+ exit 1\)\z/)
    end

    it "raises if the command doesn't exist" do
      expect do
        SystemCommand.each_line('__missing_command__') { flunk 'unexpected yield' }
      end.to raise_error(Errno::ENOENT, 'No such file or directory - __missing_command__')
    end

    it 'raises if the command is not executable' do
      expect do
        SystemCommand.each_line(not_executable) { flunk 'unexpected yield' }
      end.to raise_error(Errno::EACCES, %r{\APermission denied - .*/not_executable\z})
    end

    it 'raises if no arguments are provided' do
      expect { SystemCommand.each_line }.to raise_error(ArgumentError, 'Must specify one or more argument')
    end

    it 'raises if no block is provided' do
      expect { SystemCommand.each_line 'true' }.to raise_error(LocalJumpError, 'no block given')
    end
  end
end
