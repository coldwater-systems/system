# frozen_string_literal: true

require_relative 'lib/system_command/version'

Gem::Specification.new do |spec|
  spec.name = 'system_command'
  spec.version = SystemCommand::VERSION
  spec.authors = ['Alex Gittemeier']
  spec.email = ['me@a.lexg.dev']

  spec.summary = 'Run external process as if they were functions.'
  spec.homepage = 'https://gitlab.com/coldwater-systems/gems/system_command'
  spec.required_ruby_version = '>= 3.2.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  # spec.metadata["changelog_uri"] = "TODO: Put your gem's CHANGELOG.md URL here."

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(__dir__) do
    `git ls-files -z`.split("\x0").reject do |f|
      (File.expand_path(f) == __FILE__) || f.start_with?(*%w[bin/ test/ spec/ features/ .git .circleci appveyor])
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) {|f| File.basename(f) }
  spec.require_paths = ['lib']
  spec.metadata['rubygems_mfa_required'] = 'true'

  spec.add_dependency 'colorize', '~> 1.1'
end
