# SystemCommand

`SystemCommand` allows you to run external process as if they were functions.

## Usage

Run `gem install system_command` or add `gem 'system_command', '~> 2.0'` to your Gemfile.


Example:

```ruby
git = SystemCommand.new('git')
SystemCommand.verbose = true # if you want executed commands to be printed to stdout similar to rake.

# Runs `git status` with its output sent to the terminal. Raises SystemCommand::Error if git does not exit successfully.
git.run('status')

# Runs `git diff --exit-code` with its output sent to the terminal. Returns true if git exits successfully.
if git.run?('diff', '--exit-code')
  # Runs `git rev-parse --abbrev-ref HEAD` and returns the output. Raises SystemCommand::Error if git does not exit successfully.
  branch = git.gets('rev-parse', '--abbrev-ref', 'HEAD')
end

# more ways to capture output from commands
lines = git.lines('shortlog')

git.each_line('shortlog') do |line|
  ...
end
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rspec` to run the tests.

To install this gem onto your local machine, run `rake install`.

To release a new version:

- Switch to the `main` branch.
- Bump `lib/system_command/version.rb`, run `bundle install`, then commit the result.
- Switch to the `v2` branch.
- Run `git merge main`
- Run `rake release`, which will create/push a git tag and publish the `.gem`
  file to [rubygems.org].

[rubygems.org]: https://rubygems.org

## Contributing

Bug reports and pull requests are welcome on GitLab at <https://gitlab.com/coldwater-systems/system_command>.
